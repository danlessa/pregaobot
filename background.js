//References:
//Chrome Developer API
//http://stackoverflow.com/questions/11325415/access-iframe-content-from-a-chromes-extension-content-script
//https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Forms/Sending_forms_through_JavaScript

//################### Utils ###############################3
//############################################################################

function sendData(data, url) {
    var XHR = new XMLHttpRequest();
    var urlEncodedData = "";
    var urlEncodedDataPairs = [];
    var name;

    for (name in data) {
        urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
    }

    urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');
    XHR.open('POST', url, false);
    XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    XHR.send(urlEncodedData);
    return XHR.responseText;
}

//############################### Global vars ########################333
//#########################################################################

var n = 0;
var current_url = null;
var numero_pregao = null;
var triggered = false;
var loaded = false;

var current_item_no = null;
var item_index = null;

//var action_url = "https://treinamento.comprasnet.gov.br/pregao/fornec/inclui_lance.asp";
var action_url = "https://www.comprasnet.gov.br/pregao/fornec/inclui_lance.asp";
var data_path = "dados.csv";
var data_URI = chrome.extension.getURL(data_path);

var raw_data = null;
var loaded_data = null;
var actual_data = null;
var time_data = null;

//time in miliseconds
var time_delta = 20 * 1000;
var current_score = 0;

//############################# FUncs ######################################
//##########################################################################
//https://www.comprasnet.gov.br/pregao/fornec/gerencia_lance.asp?prgcod=643189&numprp=142016&indSRP=Sim&indICMS=N%E3o&sequence=927135
function getNumPrp() {

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(arrayOfTabs) {
        pregao_url = arrayOfTabs[0].url;
        url_r = pregao_url.split("numprp=")[1];
        numero_pregao = url_r.substring(0, url_r.indexOf("&"));

        if ((pregao_url != current_url) || (!loaded)) {
            if (initialize()) {
                refreshPage();
            }
        }
        current_url = pregao_url;
        return true;
    });
    return true;
}
//Refresh current tab
function refreshPage() {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(arrayOfTabs) {
        var code = 'window.location.reload();';
        chrome.tabs.executeScript(arrayOfTabs[0].id, {
            code: code
        });
    });
}



function testAction() {
    temp = actual_data[0];
    temp.valorlance = "26,9999"; //tem que ser nesse formato
    delete temp["estado"];
    delete temp["item_no"];
    delete temp["situacao"];
    var params = JSON.stringify(temp);
    console.log(params);
    sendData(temp, action_url);
    refreshPage();
}

//Loads data from data_URI
function loadData() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", data_URI, false);
    xhr.send();
    csv_text = xhr.responseText;
    csv = Papa.parse(csv_text, {
        "header": true
    }).data;
    columns = [];
    json = {};
    for (i = 0; i < csv.length; ++i) {
        row = csv[i];
        if (json[row.numero_pregao] == null) {
            json[row.numero_pregao] = [];
        }
        json_row = {
            "numero_pregao": row.numero_pregao,
            "item_no": row.item_no,
            "valor_minimo": row.valor_minimo
        };
        json[row.numero_pregao].push(json_row);
    }

    if (json[""] != null) {
        delete json[""];
    }
    return json;
}

function initialize() {
    raw_data = loadData();
    loaded_data = raw_data[numero_pregao];
    if (loaded_data == null) {
        alert("Atenção, o pregão atual não está cadastrado nos dados");
        return false;
    }

    loaded = true;
    item_index = 0;

    time_data = [];
    for (i = 0; i < loaded_data.length; ++i) {
        time_data.push({
            "item_no": loaded_data[i].item_no,
            "last_sent": 0
        });
    }
}

//Bot initialization routine -- triggered by hotkey
function botStart() {
    if (getNumPrp() == true) {
        chrome.browserAction.setIcon({
            path: "img/run.png"
        });
        triggered = true;
        console.log("Bot iniciado");
        refreshPage();
    } else {
        console.log("Erro ao inicializar bot, verifique se há itens abertos no pregão ou se o número do pregão está registrado entre os dados inclusos");
    }
}


//Bot exit routine -- triggered by hotkey
function botStop() {
    loaded = false;
    triggered = false;
    chrome.browserAction.setIcon({
        path: "img/stop.png"
    });
    console.log("Bot interrompido, ofertas aceitas: " + current_score);
    current_score = 0;
}

function check(item_no) {
    time_item = time_data.filter(function(x) {
        return x.item_no == item_no;
    })[0];

    loaded_item = loaded_data.filter(function(x) {
        return x.item_no == item_no;
    })[0];

    actual_item = actual_data.filter(function(x) {
        return x.item_no == item_no;
    })[0];

    str = "Item " + item_no + ": S";

    if (actual_item == null) {
        console.log(str + "0: Item não existe")
        return false;
    }

    delta_fin = parseFloat(loaded_item.valor_minimo) - parseFloat(actual_item.valormin);
    delta = Date.now() - time_item.last_sent;



    if (!actual_item.situacao.includes("Alea")) {
        console.log(str + "1: Não está no aleatório, sit. atual: " + actual_item.situacao);
        return false;
    } else if (actual_item.estado.includes("LanceVencedor")) {
        console.log(str + "2: O lance atual está vencendo");
        return false;
    } else if (time_item.last_sent < 0) {
        console.log(str + "3: Item desativado");
        return false;
    } else if (delta_fin > 0) {
        console.log(str + "4: Menor lance excede valor mínimo, diferença: " + delta_fin);
        return false;
    } else if (delta < time_delta) {
        console.log(str + "5: Não decorreu 20s, tempo decorrido (ms): " + delta);
        return false;
    } else {
        console.log(str + "#: Item válido");
        return true;
    }

}

function nextValidNo() {
    for (i = 0; i < time_data.length; ++i) {
        item_no = time_data[i].item_no;
        if (check(item_no)) {
            return {
                "item_no": item_no,
                "haveValid": true
            };
        }
    }
    return {
        "item_no": null,
        "haveValid": false
    };
}

function action(item_no) {

    actual_item = actual_data.filter(function(x) {
        return x.item_no == item_no;
    })[0];

    time_item = time_data.filter(function(x) {
        return x.item_no == item_no;
    })[0];

    //Check if we can offer something inside boundaries
    action_item = actual_item;
    delete action_item["estado"];
    delete action_item["item_no"];
    delete action_item["situacao"];
    action_item.valorlance = (action_item.valormin - 0.01).toFixed(4).toString().replace(".", ",");
    var params = JSON.stringify(action_item);
    console.log("Ofertando no Item " + item_no + " -- R$" + action_item.valorlance);
    op = sendData(action_item, action_url);

    done = op.includes("registrado+com+sucesso");

    if (done) {
        time_item.last_sent = Date.now();
        current_score += 1;
        return true;
    } else {
        console.log(op);
        return false;
    }



}

function work() {
    n += 1
    console.log("##### Iteração " + n + "(" + current_score + ")" + " | Epoch(ms):" + Date.now() + " #####");
    time_item = time_data[item_index];
    current_item_no = time_item.item_no;

    ++item_index;
    if (item_index >= time_data.length) {
        item_index = 0;
    }

    if (!check(current_item_no)) {
        out = nextValidNo();
        if (out.haveValid == true) {
            current_item_no = out.item_no;
            time_item = time_data.filter(function(x) {
                return x.item_no == current_item_no;
            })[0];
            console.log("Mudando item corrente para " + current_item_no);
        } else {
            console.log("Sem itens válidos, refresh");
            refreshPage();
            return false;
        }
    }

    current_item_no = time_item.item_no;
    now = Date.now();
    delta = now - time_item.last_sent;

    act = action(current_item_no);
    console.log("Oferta feita no item " + current_item_no + ", sucesso=(" + act + ")");
    refreshPage();
    return true;
}

//########################### Listeners ########################################
//##############################################################################


//Obvious
function hotkeyListener(command) {
    if (command == "pregao-trigger") {
        if (triggered == false) {
            botStart();
        } else {
            botStop();
        }
    }
}

chrome.runtime.onMessage.addListener(function(message, sender) {
    if (message.sendBack && triggered) {
        actual_data = message.data;
        if (triggered) {
            getNumPrp();
            work();
        }

    }
});

chrome.commands.onCommand.addListener(hotkeyListener);
