//sub.js
//Esse código faz injeção de JS nos iFrames para comunicação
//com o resto da extensão

//Verificar se a injeção está ocorrendo em um iFrame
if (!window.isTop) {

    tit = document.title;
    if(tit == "Pregão"){

        body_js = document.body.children[1].innerText;

        str_sequence = "sequence.value = ";
        n_sequence = body_js.search(str_sequence) + str_sequence.length;
        sequence_length = body_js.substr(n_sequence, body_js.length - n_sequence).indexOf(";");
        sequence = body_js.substr(n_sequence, sequence_length);

        str_ticket = "ticket.value = ";
        n_ticket = body_js.search(str_ticket) + str_ticket.length;
        ticket_length = body_js.substr(n_ticket, body_js.length - n_ticket).indexOf(";");
        ticket = body_js.substr(n_ticket, ticket_length);


        items = document.getElementsByClassName("tex3");
        var data = [];
        for (i = 0; i < items.length; ++i){
            item = items[i].getElementsByTagName("input");

            data_it = {};

            data_it.valorlance = item[0].value;
            data_it.sequence = sequence;
            data_it.ticket = ticket;
            data_it.ipgCod = item[3].value;
            data_it.prgCod = item[4].value;
            data_it.ippCod = item[5].value;
            data_it.valormin = item[6].value;
            data_it.menorlance = item[7].value;
            data_it.formajulg = item[8].value;
            data_it.fatorEqualiz = item[9].value;
            data_it.acaoLance = "ENVIAR";


            data_it.estado = items[i].children[0].children[0].src;
            data_it.item_no = items[i].children[1].innerText;
            data_it.situacao = items[i].children[4].innerText;

            data.push(data_it);
        }
        chrome.runtime.sendMessage({sendBack:true, data:data});
    }

}
